package com.example.jo.room.di

import android.content.Context
import com.example.jo.room.App
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun provideContext(app: App): Context

}