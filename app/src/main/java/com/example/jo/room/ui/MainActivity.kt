package com.example.jo.room.ui

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.jo.room.R
import com.example.jo.room.db.entities.Models
//import com.example.jo.room.db.entities.GroupEntity
//import com.example.jo.room.db.entities.Post
import com.example.jo.room.db.entities.PostEntity
import com.example.jo.room.db.entities.ProfileEntity
//import com.example.jo.room.db.entities.UserEntity
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers
import io.requery.Persistable
import io.requery.reactivex.KotlinReactiveEntityStore
import java.util.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var db: KotlinReactiveEntityStore<Persistable>

    private val result: Button by lazy {
        findViewById<Button>(R.id.get_result)
    }
    private val postInput: EditText by lazy {
        findViewById<EditText>(R.id.post_input)
    }
    private val post: Button by lazy {
        findViewById<Button>(R.id.create_post)
    }
    private val addUser: Button by lazy {
        findViewById<Button>(R.id.add_user)
    }
    private val addGroup: Button by lazy {
        findViewById<Button>(R.id.add_group)
    }
    private val deleteAllUsers: Button by lazy {
        findViewById<Button>(R.id.delete_all_user)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        result.setOnClickListener {
            getResult()
        }

        post.setOnClickListener {
            createPost()
        }

        addUser.setOnClickListener {
            insertNewUsers()
        }

        addGroup.setOnClickListener {
            createGroup()
        }

        deleteAllUsers.setOnClickListener {
            removeAllUsers()
        }

    }

    private fun removeAllUsers() {
        Thread({
//            Models.DEFAULT.

            db.delete(ProfileEntity::class)
                    .get()
                    .value()
        }).start()
    }

    private fun createGroup() {
        db.select(ProfileEntity::class)
                .get()
                .observable()
                .toList()
                .toObservable()
//                .concatMap {
//                    val entity = GroupEntity()
//                    entity.name = "test"
//                    entity.users = setOf(it[Random().nextInt(it.lastIndex)])
//                    db.insert(entity).toObservable()
//                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.d(tag, it.toString())
                        },
                        {
                            Log.e(tag, it.message, it)
                        }
                )
    }

    private fun createPost() {
        val text = postInput.text.toString()
        if (text.isEmpty()) {
            return
        }

        db.select(ProfileEntity::class)
                .get()
                .observable()
                .toList()
                .toObservable()
                .concatMap {
                    val entity = PostEntity()
                    entity.text = text
                    entity.createBy = it[Random().nextInt(it.lastIndex)]
                    db.insert(entity).toObservable()
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            postInput.text.clear()
                            Log.d(tag, it.toString())
                        },
                        {
                            Log.e(tag, it.message, it)
                        }
                )
    }

    private fun getResult() {
        db.select(ProfileEntity::class)
                .get()
                .observable()
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.d(tag, it.toString())
                        },
                        {
                            Log.e(tag, it.message, it)
                        }
                )
    }

    private fun insertNewUsers() {
//        val user1 = ProfileEntity().apply {
//            id = 4
//            name = "update"
////            phone = "1234321"
//        }

        val user1 = ProfileEntity().apply {
            id = 11
            name = "User${Random().nextInt(100)}"
//            phone = "12344321"
            phone = null
        }

//        val user1 = UserEntity()
//        user1.name = "User${Random().nextInt(100)}"
//        val postEntity = PostEntity()
//        postEntity.text = "123"
//        postEntity.createBy = user1
//        user1.posts = setOf(postEntity)

        user1.toSingle()
                .flatMap {
                    db.upsert(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Log.d(tag, it.toString())
                        },
                        {
                            Log.e(tag, it.message, it)
                        }
                )
    }

    companion object {
        val tag = "main_activity"
    }

}
