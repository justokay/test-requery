package com.example.jo.room.db.entities

import io.requery.Column
import io.requery.Entity
import io.requery.Key
import io.requery.Table

@Table(name = "schedule")
@Entity(name = "ScheduleEntity")
open class Schedule() {

    @get:Key()
    @get:Column(name = "_ID")
    var id: Long = 0
    @get:Column(name = "name")
    var name: String = ""

}