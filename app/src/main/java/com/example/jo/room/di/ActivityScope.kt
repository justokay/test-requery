package com.example.jo.room.di

import javax.inject.Scope

@Scope
annotation class ActivityScope
