package com.example.jo.room.db.entities

import io.requery.*

@Table(name = "USER")
@Entity(name = "ProfileEntity")
interface Profile {

    @get:Key()
    @get:Generated
    @get:Column(name = "_ID")
    var id: Long
    @get:Column(name = "NAME")
    var name: String
    @get:Column(name = "PHONE")
    var phone: String?
    @get:OneToMany(mappedBy = "CREATE_BY")
    var posts: Set<Post>

//    @get:ManyToMany
//    var groups: Set<Group>

}