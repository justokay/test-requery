package com.example.jo.room.db.entities

import io.requery.*

//@Table(name = "TEAM")
//@Entity(name = "GroupEntity")
interface Group {

    @get:Generated
    @get:Key
    @get:Column(name = "_ID")
    val id: Long
    @get:Column(name = "NAME")
    var name: String

//    @get:JunctionTable(name = "GROUP_USER")
//    @get:ManyToMany
//    var users: Set<Profile>

}