package com.example.jo.room.di

import android.content.Context
import com.example.jo.room.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            DBModule::class,
            ActivityBindingModule::class
        ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    val context: Context

    fun inject(app: App)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(appComponent: App): AppComponent.Builder

        fun build(): AppComponent
    }

}