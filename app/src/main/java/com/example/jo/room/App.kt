package com.example.jo.room

import com.example.jo.room.di.AppComponent
import com.example.jo.room.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build()
        appComponent.inject(this)
        return appComponent
    }


    companion object {

        @JvmStatic
        lateinit var appComponent: AppComponent
            private set
    }

}