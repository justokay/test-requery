package com.example.jo.room.db.entities

import io.requery.*

//@Table(name = "POST_COMMENT")
//@Entity(name = "PostCommentEntity")
interface PostComment {

    @get:Key()
    @get:Generated
    @get:Column(name = "_ID")
    val id: Long
    @get:Column(name = "TEXT")
    var text: String
    @get:Column(name = "POST_ID")
    var postId: Long
    @get:Column(name = "CREATE_BY")
    var userId: Long

}