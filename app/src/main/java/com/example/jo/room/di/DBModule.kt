package com.example.jo.room.di

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.jo.room.db.entities.Models
import com.example.jo.room.db.entities.ProfileEntity
import dagger.Module
import dagger.Provides
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import javax.inject.Singleton

@Module
class DBModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): KotlinReactiveEntityStore<Persistable> {
        val source = object : DatabaseSource(context, Models.DEFAULT, "database.db", 3) {
            override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
                super.onUpgrade(db, oldVersion, newVersion)

                if (oldVersion == 2 && newVersion < 3) {
                    db?.execSQL("alter table ${ProfileEntity.`$TYPE`.name} add ${ProfileEntity.PHONE.name} text")
                }
            }
        }
        source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS)
        return KotlinReactiveEntityStore(KotlinEntityDataStore(source.configuration))
    }

}