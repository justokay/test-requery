package com.example.jo.room.db.entities

import io.requery.*

@Table(name = "POST")
@Entity(name = "PostEntity")
interface Post {
    @get:Generated
    @get:Key()
    @get:Column(name = "_ID")
    val id: Long
    @get:Column(name = "TEXT")
    var text: String
    @get:ManyToOne()
    @get:Column(name = "CREATE_BY")
    var createBy: Profile
    @get:Column(name = "CREATE_DATE")
    var date: Long
}